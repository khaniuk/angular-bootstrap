import { UserModel } from './user.model';

export interface UserResponseModel {
  list?: Array<UserModel>;
}
