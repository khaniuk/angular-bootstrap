export interface UserModel {
  id: number;
  name: string;
  email: string;
  password: string;
  phone: number;
  status: boolean;
  languages?: Array<Language>;
}

interface Language {
  name: string;
}
