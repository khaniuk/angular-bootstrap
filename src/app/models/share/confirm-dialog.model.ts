export interface ConfirmDialogModel {
  icon?: string;
  message: string;
}
