import { ActionEnum } from './crud-enum';

export interface DialogModel {
  formType: ActionEnum;
  record?: any;
}
