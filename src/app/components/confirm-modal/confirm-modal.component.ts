import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent implements OnInit {
  title: string;
  modalOptions: any;

  @Output() confirmAction = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    public bsModalOptions: ModalOptions
  ) {
    this.title = '';
  }

  ngOnInit(): void {
    this.modalOptions = this.bsModalOptions.initialState;
    this.title = this.modalOptions.title;
  }

  confirmModal() {
    this.confirmAction.emit(true);
    this.bsModalRef.hide();
  }

  cancelModal() {
    this.bsModalRef.hide();
  }
}
