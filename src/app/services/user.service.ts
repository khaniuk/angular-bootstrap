import { Injectable } from '@angular/core';

import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  defaultUser: Array<UserModel>;
  constructor() {
    this.defaultUser = [
      {
        id: 1,
        name: 'Lewis Lengs',
        email: 'lewis@mail.com',
        phone: 123456789,
        password: '123456789',
        status: true,
        languages: [{ name: 'Spanish' }, { name: 'English' }],
      },
      {
        id: 2,
        name: 'Lanna Mars',
        email: 'lanna@mail.com',
        phone: 234567890,
        password: '123456789',
        status: true,
        languages: [{ name: 'Spanish' }, { name: 'English' }],
      },
      {
        id: 3,
        name: 'Louren Mopher',
        email: 'louren@mail.com',
        phone: 345678901,
        password: '123456789',
        status: true,
        languages: [{ name: 'Spanish' }, { name: 'English' }],
      },
      {
        id: 4,
        name: 'Paul Roldan',
        email: 'paul@mail.com',
        phone: 456789012,
        password: '123456789',
        status: true,
        languages: [{ name: 'Spanish' }, { name: 'English' }],
      },
      {
        id: 5,
        name: 'Linus Torvalds',
        email: 'linus@mail.com',
        phone: 567890123,
        password: '123456789',
        status: true,
        languages: [{ name: 'English' }],
      },
    ];

    sessionStorage.setItem('users', JSON.stringify(this.defaultUser));
  }

  async getUsers() {
    let data: Array<UserModel> = [];
    let users: any = sessionStorage.getItem('users');
    if (users) {
      data = await JSON.parse(users);
    }
    return data;
  }

  async saveUser(body: UserModel) {
    let id: number = 1;
    let data: Array<UserModel> = [];
    try {
      const users = sessionStorage.getItem('users');
      if (users) {
        data = await JSON.parse(users);
        id = data[data.length - 1].id + 1;
      }
      body.id = id;
      data.push(body);
      sessionStorage.setItem('users', JSON.stringify(data));
      return true;
    } catch (error) {
      return false;
    }
  }

  async putUser(body: UserModel) {
    let position;
    let data: Array<UserModel> = [];
    try {
      const users = sessionStorage.getItem('users');
      if (users) {
        data = await JSON.parse(users);
        position = data.findIndex((record) => record.id == body.id);
        if (position != -1) {
          data[position] = body;
        }
        sessionStorage.setItem('users', JSON.stringify(data));
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  async deleteUser(id: number) {
    let position;
    let data: Array<UserModel> = [];
    try {
      const users = sessionStorage.getItem('users');
      if (users) {
        data = await JSON.parse(users);
        position = data.findIndex((record) => record.id == id);
        if (position != -1) {
          data.splice(position, 1);
        }
        sessionStorage.setItem('users', JSON.stringify(data));
      }
      return true;
    } catch (error) {
      return false;
    }
  }
}
