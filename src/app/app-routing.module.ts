import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './views/dashboard/dashboard.component';
import { InstructionComponent } from './views/instruction/instruction.component';
import { UserComponent } from './views/user/user.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'instruction', component: InstructionComponent },
  { path: 'user', component: UserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
