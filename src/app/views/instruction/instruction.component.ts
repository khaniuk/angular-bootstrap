import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.scss'],
})
export class InstructionComponent implements OnInit {
  components: Array<{ checked: boolean; description: string }>;

  constructor() {
    this.components = [
      { checked: true, description: 'Menu' },
      { checked: true, description: 'Tabs' },
      { checked: true, description: 'Table' },
      { checked: true, description: 'Paginator' },
      { checked: true, description: 'Search' },
      { checked: false, description: 'FormControl' },
      { checked: true, description: 'Input' },
      { checked: true, description: 'Select' },
      { checked: true, description: 'Checkbox' },
      { checked: true, description: 'Button' },
      { checked: true, description: 'Dialog' },
    ];
  }

  ngOnInit(): void {}
}
