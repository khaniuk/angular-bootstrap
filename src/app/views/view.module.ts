import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapModule } from '../bootstrap.module';

import { InstructionComponent } from './instruction/instruction.component';
import { UserComponent } from './user/user.component';

import { UserFormComponent } from './user/user-form/user-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmModalComponent } from '../components/confirm-modal/confirm-modal.component';

import { OnlyNumberDirective } from '../directives/only-number.directive';
import { OnlyTextDirective } from '../directives/only-text.directive';

@NgModule({
  declarations: [
    InstructionComponent,
    UserComponent,
    UserFormComponent,
    ConfirmModalComponent,

    OnlyNumberDirective,
    OnlyTextDirective,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, BootstrapModule],
})
export class ViewModule {}
