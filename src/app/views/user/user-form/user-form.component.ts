import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { ActionEnum } from 'src/app/models/share/crud-enum';
import { UserModel } from 'src/app/models/user.model';

import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  form: FormGroup;
  modalOptions: any;
  title: string;

  @Output() modalAction = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    public bsModalOptions: ModalOptions,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.title = '';
    this.form = new FormGroup({});
  }

  ngOnInit(): void {
    this.modalOptions = this.bsModalOptions.initialState;
    this.title = this.modalOptions.title;

    if (this.modalOptions.typeForm === ActionEnum.CREATE) {
      this.form = this.formBuilder.group({
        id: [null],
        name: [
          null,
          [
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(30),
          ],
        ],
        email: [null, [Validators.required]],
        password: [null, [Validators.required]],
        phone: [null, [Validators.required]],
        status: [true, [Validators.required]],
        //languages: [[], [Validators.required]],
      });
    } else {
      const selectedRow: UserModel = this.modalOptions.data;
      this.form = this.formBuilder.group({
        id: [selectedRow.id],
        name: [
          selectedRow.name,
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(30),
          ],
        ],
        email: [selectedRow.email, [Validators.required]], // Validators.pattern(RegularExp.email),
        password: [selectedRow.password, [Validators.required]],
        phone: [selectedRow.phone, [Validators.required]],
        status: [selectedRow.status, [Validators.required]],
        //languages: [selectedRow.languages, [Validators.required]],
      });
    }
  }

  saveUser() {
    if (this.form.valid) {
      // this.btnLoading = true;
      const newRecord: UserModel = {
        ...this.form.value,
      };

      if (this.modalOptions.typeForm === ActionEnum.CREATE) {
        this.userService
          .saveUser(newRecord)
          .then((response: boolean) => {
            // this.btnLoading = false;
            if (response) {
              this.hideModal();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        this.userService
          .putUser(newRecord)
          .then((response: boolean) => {
            // this.btnLoading = false;
            if (response) {
              this.hideModal();
            }
          })
          .catch((err) => {
            // this.btnLoading = false;
            console.log(err);
          });
      }
    } else {
      this.toastr.info('Information not valid');
    }
  }

  hideModal() {
    this.modalAction.emit(true);
    this.bsModalRef.hide();
  }

  closeModal() {
    this.bsModalRef.hide();
  }
}
