import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/user.model';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

import { UserFormComponent } from './user-form/user-form.component';
import { ConfirmModalComponent } from 'src/app/components/confirm-modal/confirm-modal.component';

import { ActionEnum } from 'src/app/models/share/crud-enum';
import { Messages } from 'src/app/utils/message';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  searchText: string;
  users: Array<UserModel>;
  bsModalRef?: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.searchText = '';
    this.users = [];
  }

  ngOnInit(): void {
    //this.userService.setUsers();
    this.getUser();
  }

  getUser() {
    this.userService.getUsers().then((response: Array<UserModel>) => {
      this.users = response;
    });
  }

  modalAddUser() {
    const initialState: ModalOptions = {
      initialState: {
        typeForm: ActionEnum.CREATE,
        title: 'ADD RECORD',
        data: null,
      },
    };
    this.bsModalRef = this.modalService.show(UserFormComponent, initialState);
    this.bsModalRef.content.modalAction.subscribe((response: boolean) => {
      if (response) {
        this.toastr.success(Messages.MSG_ADD);
        this.getUser();
      } else {
        this.toastr.error(Messages.MSG_NOT_ADD);
      }
    });
  }

  modalUpdateUser(rowData: UserModel): void {
    const initialState: ModalOptions = {
      initialState: {
        typeForm: ActionEnum.UPDATE,
        title: 'UPDATE RECORD',
        data: rowData,
      },
    };
    this.bsModalRef = this.modalService.show(UserFormComponent, initialState);
    this.bsModalRef.content.modalAction.subscribe((response: number) => {
      if (response) {
        this.toastr.success(Messages.MSG_UPD);
        this.getUser();
      } else {
        this.toastr.error(Messages.MSG_NOT_UPD);
      }
    });
  }

  deleteUser(rowData: UserModel) {
    const initialState: ModalOptions = {
      initialState: {
        typeForm: ActionEnum.DELETE,
        title: 'DELETE RECORD',
        data: rowData,
      },
    };
    this.bsModalRef = this.modalService.show(
      ConfirmModalComponent,
      initialState
    );
    this.bsModalRef.content.confirmAction.subscribe((response: number) => {
      if (response) {
        this.userService
          .deleteUser(rowData.id)
          .then((response: boolean) => {
            if (response) {
              this.toastr.success(Messages.MSG_DEL);
              this.getUser();
            } else {
              this.toastr.error(Messages.MSG_NOT_DEL);
            }
          })
          .catch((err) => {
            this.toastr.error(Messages.MSG_NOT_DEL);
          });
      }
    });
  }
}
